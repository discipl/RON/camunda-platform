# Camunda

This repository contains the files required to host a [Camunda Platform](https://docs.camunda.org/manual/latest/installation/docker/) demo.

Please note that the setup in this repository is made for demo purposes only. It should not be used as a production setup.

## Install on localhost

Clone this project and run docker-compose which will start MySQL and Cmaunda Platform

```
~/$ git clone git@gitlab.com:discipl/RON/camunda-platform.git
~/$ cd Camunda-VIL-regels/
~/Camunda-for-Beginners (master) $ docker-compose up -d
```

### Tomcat

Open `http://localhost:8080/` in your browser to acces Apache Tomcat.

![Apache Tomcat](./images/Tomcat.png)

### Cockpit, Tasklist and Admin

Open `http://localhost:8080/camunda/app/welcome/default/#!/login` and login with demo/demo to access Camunda applications.

![Applications](./images/Applications.png)

### REST API

The REST API is included in Camunda’s pre-built distributions. It can be accessed via the context /engine-rest and uses the engines provided by the class BpmPlatform.

The default process engine is available out of the box by accessing `/engine-rest/engine/default/{rest-methods}` or simply `/engine-rest/{rest-methods}`.

So, for example, the [GET /decision-definition](https://docs.camunda.org/manual/7.16/reference/rest/decision-definition/get-query/#method) queries for decision definitions that fulfill given parameters. Here is an Postman example of the output with pre-installed examples.

![Postman](./images/Postman.png)

## Install on server

For demo purposes a server has been installed on the domain `vil-regels.nl`. For this server we followed the [Install the Full Distribution on a Tomcat Application Server manually](https://docs.camunda.org/manual/latest/installation/full/tomcat/manual/) instruction. We completed the setup adding TLS using Let’s Encrypt SSL with the Tomcat web server.


The Camunda Platform can now be found there with the [VIL_IIT_Utrecht v1.0.6](https://gitlab.com/discipl/RON/eclipse/-/blob/master/viliit-dmn/src/main/resources/IITv1.0.6.dmn) DMN table deployed.

![vil-regels.nl](./images/vil-regels.png)

# OpenAPI

[inducoapi 2.0.0](https://pypi.org/project/inducoapi/) has been used to to realize OpenAPI spec for the `POST /evaluate` request in a very simple way.

```
Camunda-VIL-regels (master) $ cd InducOapi/
Camunda-VIL-regels/InducOapi (master) $ python -m inducoapi POST /evaluate 200 --request request.json --response response.json --output openapi.yaml
```
